/*==============================================================*/
/* Table: t_sys_role_form                                     */
/*==============================================================*/
create table t_sys_role_form
(
  id                 varchar2(40)         not null,
  role_id            varchar2(40)         not null,
  form_code          varchar2(40),
  form_name          varchar2(40),
  create_date        DATE                 not null,
  last_update_date   DATE,
  create_by          varchar2(40),
  last_update_by     varchar2(40),
  sys_org_code       varchar2(40),
  sys_owner_cpy      varchar2(40),
  rmk                varchar2(1000),
  enable_flag        CHAR(1),
  attribute1         varchar2(100),
  attribute2         varchar2(100),
  version            number(8),
  constraint PK_T_SYS_ROLE_FORM primary key (id)
);

comment on table t_sys_role_form is
'角色表单关系表';

comment on column t_sys_role_form.id is
'主键(sys_code+UUID)';

comment on column t_sys_role_form.role_id is
'角色 ID';

comment on column t_sys_role_form.form_code is
'表单系统编号';

comment on column t_sys_role_form.form_name is
'表单名称';

comment on column t_sys_role_form.last_update_date is
'最后更新时间';

comment on column t_sys_role_form.last_update_by is
'最后更新人';

comment on column t_sys_role_form.sys_org_code is
'用户所属组织部门';

comment on column t_sys_role_form.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_role_form.rmk is
'备注';

comment on column t_sys_role_form.enable_flag is
'有效标识';

comment on column t_sys_role_form.attribute1 is
'扩展属性1 ';

comment on column t_sys_role_form.attribute2 is
'扩展属性二';

comment on column t_sys_role_form.version is
'更新版本号';

-- Create/Recreate indexes
create index SYS_ROLE_FORM_idx_roleid on T_SYS_ROLE_FORM (role_id);

/*==============================================================*/
/* Table: t_form_set_main                                     */
/*==============================================================*/
create table t_form_set_main
(
  id                 varchar2(40)         not null,
  form_name          varchar2(100),
  form_type          varchar2(20),
  oa_menu_name       varchar2(200),
  module_bus_class   varchar2(20),
  form_view_url      varchar2(200),
  form_add_url       varchar2(200),
  form_edit_url      varchar2(200),
  form_delete_url    varchar2(200),
  create_date        DATE                 not null,
  last_update_date   DATE,
  create_by          varchar2(40),
  last_update_by     varchar2(40),
  sys_org_code       varchar2(40),
  sys_owner_cpy      varchar2(40),
  rmk                varchar2(1000),
  enable_flag        CHAR(1),
  attribute1         varchar2(100),
  attribute2         varchar2(100),
  version            number(8),
  constraint PK_T_FORM_SET_MAIN primary key (id)
);
-- Add/modify columns
alter table T_FORM_SET_MAIN add form_width number(3);
-- Add comments to the columns
comment on column T_FORM_SET_MAIN.form_width
is '表单宽度百分比';

comment on table t_form_set_main is
'表单设计主表';

comment on column t_form_set_main.id is
'主键(sys_code+UUID)';

comment on column t_form_set_main.form_name is
'表单名称';

comment on column t_form_set_main.form_type is
'表单类型(OA:申请, EXT:用于模块扩展, OTHER:其它任务申请)';

comment on column t_form_set_main.oa_menu_name is
'OA菜单分类名称';

comment on column t_form_set_main.module_bus_class is
'业务模块类型(对应t_sys_menu.bus_module_class)';

comment on column t_form_set_main.form_view_url is
' 表单查看地址';

comment on column t_form_set_main.form_add_url is
'表单新增地址';

comment on column t_form_set_main.form_edit_url is
'表单编辑后台地址';

comment on column t_form_set_main.form_delete_url is
'表单删除地址';

comment on column t_form_set_main.last_update_date is
'最后更新时间';

comment on column t_form_set_main.last_update_by is
'最后更新人';

comment on column t_form_set_main.sys_org_code is
'用户所属组织部门';

comment on column t_form_set_main.sys_owner_cpy is
'用户所属公司';

comment on column t_form_set_main.rmk is
'备注';

comment on column t_form_set_main.enable_flag is
'有效标识';

comment on column t_form_set_main.attribute1 is
'扩展属性1 ';

comment on column t_form_set_main.attribute2 is
'扩展属性二';

comment on column t_form_set_main.version is
'更新版本号';

/*==============================================================*/
/* Table: t_form_set_content                                  */
/*==============================================================*/
create table t_form_set_content
(
  id                 varchar2(40)         not null,
  main_id            varchar2(40),
  form_content       clob,
  form_html          clob,
  form_json          clob,
  form_js            clob,
  create_date        DATE                 not null,
  last_update_date   DATE,
  create_by          varchar2(40),
  last_update_by     varchar2(40),
  sys_org_code       varchar2(40),
  sys_owner_cpy      varchar2(40),
  rmk                varchar2(1000),
  enable_flag        CHAR(1),
  attribute1         varchar2(100),
  attribute2         varchar2(100),
  version            number(8),
  constraint PK_T_FORM_SET_CONTENT primary key (id)
);

comment on table t_form_set_content is
'表单设计内容';

comment on column t_form_set_content.id is
'主键(sys_code+UUID)';

comment on column t_form_set_content.main_id is
'主表ID';

comment on column t_form_set_content.form_content is
'表单编号';

comment on column t_form_set_content.form_html is
'表单名称';

comment on column t_form_set_content.form_json is
'表单JSON串';

comment on column t_form_set_content.last_update_date is
'最后更新时间';

comment on column t_form_set_content.last_update_by is
'最后更新人';

comment on column t_form_set_content.sys_org_code is
'用户所属组织部门';

comment on column t_form_set_content.sys_owner_cpy is
'用户所属公司';

comment on column t_form_set_content.rmk is
'备注';

comment on column t_form_set_content.enable_flag is
'有效标识';

comment on column t_form_set_content.attribute1 is
'扩展属性1 ';

comment on column t_form_set_content.attribute2 is
'扩展属性二';

comment on column t_form_set_content.version is
'更新版本号';

-- Create/Recreate indexes
create unique index FORM_SET_CONTENT_idx_mainid on T_FORM_SET_CONTENT (main_id);

/*==============================================================*/
/* Table: t_form_set_field                                    */
/*==============================================================*/
create table t_form_set_field
(
  id                 varchar2(40)         not null,
  main_id            varchar2(40),
  field_type         varchar2(15),
  field_control_id   varchar2(100),
  field_name         varchar2(100),
  field_default_value varchar2(1000),
  field_list         varchar2(1000),
  field_attr1        varchar2(1000),
  field_attr2        varchar2(1000),
  priv_flag          CHAR(1),
  require_flag       CHAR(1),
  css_attribute      varchar2(1000),
  js_attribute       varchar2(1000),
  create_date        DATE                 not null,
  last_update_date   DATE,
  create_by          varchar2(40),
  last_update_by     varchar2(40),
  sys_org_code       varchar2(40),
  sys_owner_cpy      varchar2(40),
  rmk                varchar2(1000),
  enable_flag        CHAR(1),
  attribute1         varchar2(100),
  attribute2         varchar2(100),
  version            number(8),
  constraint PK_T_FORM_SET_FIELD primary key (id)
);

comment on table t_form_set_field is
'表单设计字段定义';

comment on column t_form_set_field.id is
'主键(sys_code+UUID)';

comment on column t_form_set_field.main_id is
'主表ID';

comment on column t_form_set_field.field_type is
'控件类型(input,select,radio,checkbox)';

comment on column t_form_set_field.field_control_id is
'控件ID';

comment on column t_form_set_field.field_name is
'控件名称';

comment on column t_form_set_field.field_default_value is
'控件默认值';

comment on column t_form_set_field.field_list is
'列表选项';

comment on column t_form_set_field.priv_flag is
'权限标识';

comment on column t_form_set_field.require_flag is
'是否必填';

comment on column t_form_set_field.css_attribute is
'css控制属性';

comment on column t_form_set_field.js_attribute is
'js属性';

comment on column t_form_set_field.last_update_date is
'最后更新时间';

comment on column t_form_set_field.last_update_by is
'最后更新人';

comment on column t_form_set_field.sys_org_code is
'用户所属组织部门';

comment on column t_form_set_field.sys_owner_cpy is
'用户所属公司';

comment on column t_form_set_field.rmk is
'备注';

comment on column t_form_set_field.enable_flag is
'有效标识';

comment on column t_form_set_field.attribute1 is
'扩展属性1 ';

comment on column t_form_set_field.attribute2 is
'扩展属性二';

comment on column t_form_set_field.version is
'更新版本号';


-- Create/Recreate indexes
create index form_set_field_idx_mainid on T_FORM_SET_FIELD (main_id);

/*==============================================================*/
/* Table: t_form_set_field_priv                               */
/*==============================================================*/
create table t_form_set_field_priv
(
  id                 varchar2(40)         not null,
  main_id            varchar2(40),
  field_id           varchar2(40),
  field_control_id   varchar2(100),
  priv_type          varchar2(10),
  type_code          varchar2(20),
  type_value         varchar2(50),
  create_date        DATE                 not null,
  last_update_date   DATE,
  create_by          varchar2(40),
  last_update_by     varchar2(40),
  sys_org_code       varchar2(40),
  sys_owner_cpy      varchar2(40),
  rmk                varchar2(1000),
  enable_flag        CHAR(1),
  attribute1         varchar2(100),
  attribute2         varchar2(100),
  version            number(8),
  constraint PK_T_FORM_SET_FIELD_PRIV primary key (id)
);

comment on table t_form_set_field_priv is
'表单设计字段权限定义';

comment on column t_form_set_field_priv.id is
'主键(sys_code+UUID)';

comment on column t_form_set_field_priv.main_id is
'主表ID';

comment on column t_form_set_field_priv.field_id is
'控件定义ID';

comment on column t_form_set_field_priv.priv_type is
'权限类别(可见/visible,可编辑/edit)';

comment on column t_form_set_field_priv.type_code is
'权限定义code( ALL:所有人, USER: 用户: ROLE:角色)';

comment on column t_form_set_field_priv.type_value is
'对应type-code的值如用户账号或角色ID';

comment on column t_form_set_field_priv.last_update_date is
'最后更新时间';

comment on column t_form_set_field_priv.last_update_by is
'最后更新人';

comment on column t_form_set_field_priv.sys_org_code is
'用户所属组织部门';

comment on column t_form_set_field_priv.sys_owner_cpy is
'用户所属公司';

comment on column t_form_set_field_priv.rmk is
'备注';

comment on column t_form_set_field_priv.enable_flag is
'有效标识';

comment on column t_form_set_field_priv.attribute1 is
'扩展属性1 ';

comment on column t_form_set_field_priv.attribute2 is
'扩展属性二';

comment on column t_form_set_field_priv.version is
'更新版本号';

-- Create/Recreate indexes
create index FORM_SET_FIELD_PRIV_idx_mainid on T_FORM_SET_FIELD_PRIV (main_id);
create index FORM_SET_FIELD_PRIV_idx_fieid on T_FORM_SET_FIELD_PRIV (field_id);

/*==============================================================*/
/* Table: t_form_set_field_list                               */
/*==============================================================*/
create table t_form_set_field_list
(
  id                 varchar2(40)         not null,
  main_id            varchar2(40),
  field_control_id   varchar2(40),
  value              varchar2(100),
  label              varchar2(200),
  parent_value       varchar2(100),
  create_date        DATE                 not null,
  last_update_date   DATE,
  create_by          varchar2(40),
  last_update_by     varchar2(40),
  sys_org_code       varchar2(40),
  sys_owner_cpy      varchar2(40),
  rmk                varchar2(1000),
  enable_flag        CHAR(1),
  attribute1         varchar2(100),
  attribute2         varchar2(100),
  version            number(8),
  constraint PK_T_FORM_SET_FIELD_LIST primary key (id)
);

comment on table t_form_set_field_list is
'表单设计字段选择列表定义';

comment on column t_form_set_field_list.id is
'主键(sys_code+UUID)';

comment on column t_form_set_field_list.main_id is
'主表ID';

comment on column t_form_set_field_list.field_control_id is
'控件ID';

comment on column t_form_set_field_list.value is
'控件名称';

comment on column t_form_set_field_list.label is
'控件默认值';

comment on column t_form_set_field_list.parent_value is
'列表选项';

comment on column t_form_set_field_list.last_update_date is
'最后更新时间';

comment on column t_form_set_field_list.last_update_by is
'最后更新人';

comment on column t_form_set_field_list.sys_org_code is
'用户所属组织部门';

comment on column t_form_set_field_list.sys_owner_cpy is
'用户所属公司';

comment on column t_form_set_field_list.rmk is
'备注';

comment on column t_form_set_field_list.enable_flag is
'有效标识';

comment on column t_form_set_field_list.attribute1 is
'扩展属性1 ';

comment on column t_form_set_field_list.attribute2 is
'扩展属性二';

comment on column t_form_set_field_list.version is
'更新版本号';

-- Create/Recreate indexes
create index form_set_field_list_idx_mainid on T_FORM_SET_FIELD_LIST (main_id, field_control_id);

/* 表单申请 sql*/
/*==============================================================*/
/* Table: t_form_apply_main                                   */
/*==============================================================*/
create table t_form_apply_main
(
  id                 varchar2(40)         not null,
  form_id            varchar2(40),
  apply_name         varchar2(200),
  apply_code         varchar2(40),
  form_type          varchar2(20),
  form_code          varchar2(40),
  form_name          varchar2(100),
  form_view_url      varchar2(200),
  form_add_url       varchar2(200),
  form_edit_url      varchar2(200),
  form_delete_url    varchar2(200),
  SELF_STATE           varchar2(10),
  create_date        DATE                 not null,
  last_update_date   DATE,
  create_by          varchar2(40),
  last_update_by     varchar2(40),
  sys_org_code       varchar2(40),
  sys_owner_cpy      varchar2(40),
  rmk                varchar2(1000),
  enable_flag        CHAR(1),
  attribute1         varchar2(100),
  attribute2         varchar2(100),
  version            number(8),
  constraint PK_T_FORM_APPLY_MAIN primary key (id)
);

comment on table t_form_apply_main is
'表单申请业务数据主表';

comment on column t_form_apply_main.id is
'主键(sys_code+UUID)';

comment on column t_form_apply_main.form_id is
'表单编号';

comment on column t_form_apply_main.apply_name is
'申请名称';

comment on column t_form_apply_main.apply_code is
'系统申请编号';

comment on column t_form_apply_main.form_type is
'表单类型';

comment on column t_form_apply_main.form_code is
'系统表单编号';

comment on column t_form_apply_main.form_name is
'表单名称';

comment on column t_form_apply_main.form_view_url is
' 表单查看地址';

comment on column t_form_apply_main.form_add_url is
'表单新增地址';

comment on column t_form_apply_main.form_edit_url is
'表单编辑后台地址';

comment on column t_form_apply_main.form_delete_url is
'表单删除地址';

comment on column t_form_apply_main.SELF_STATE is
'业务状态';

comment on column t_form_apply_main.last_update_date is
'最后更新时间';

comment on column t_form_apply_main.last_update_by is
'最后更新人';

comment on column t_form_apply_main.sys_org_code is
'用户所属组织部门';

comment on column t_form_apply_main.sys_owner_cpy is
'用户所属公司';

comment on column t_form_apply_main.rmk is
'备注';

comment on column t_form_apply_main.enable_flag is
'有效标识';

comment on column t_form_apply_main.attribute1 is
'扩展属性1 ';

comment on column t_form_apply_main.attribute2 is
'扩展属性二';

comment on column t_form_apply_main.version is
'更新版本号';

/*==============================================================*/
/* Index: form_apply_main_idx_formcode                        */
/*==============================================================*/
create index form_apply_main_idx_formcode on t_form_apply_main (
  sys_owner_cpy ASC,
  form_code ASC,
  SELF_STATE ASC,
  create_by ASC
);

/*==============================================================*/
/* Table: t_form_apply_field_value                            */
/*==============================================================*/
create table t_form_apply_field_value
(
  id                 varchar2(40)         not null,
  main_id            varchar2(40),
  field_control_id   varchar2(40),
  field_name         varchar2(100),
  field_value        varchar2(1000),
  field_value_label  varchar2(1000),
  create_date        DATE                 not null,
  last_update_date   DATE,
  create_by          varchar2(40),
  last_update_by     varchar2(40),
  sys_org_code       varchar2(40),
  sys_owner_cpy      varchar2(40),
  rmk                varchar2(1000),
  enable_flag        CHAR(1),
  attribute1         varchar2(100),
  attribute2         varchar2(100),
  version            number(8),
  constraint PK_T_FORM_APPLY_FIELD_VALUE primary key (id)
);

comment on table t_form_apply_field_value is
'表单申请字段值';

comment on column t_form_apply_field_value.id is
'主键(sys_code+UUID)';

comment on column t_form_apply_field_value.main_id is
'主表ID';

comment on column t_form_apply_field_value.field_control_id is
'控件ID';

comment on column t_form_apply_field_value.field_name is
'控件名称';

comment on column t_form_apply_field_value.field_value is
'控件值';

comment on column t_form_apply_field_value.field_value_label is
'值显示值';

comment on column t_form_apply_field_value.last_update_date is
'最后更新时间';

comment on column t_form_apply_field_value.last_update_by is
'最后更新人';

comment on column t_form_apply_field_value.sys_org_code is
'用户所属组织部门';

comment on column t_form_apply_field_value.sys_owner_cpy is
'用户所属公司';

comment on column t_form_apply_field_value.rmk is
'备注';

comment on column t_form_apply_field_value.enable_flag is
'有效标识';

comment on column t_form_apply_field_value.attribute1 is
'扩展属性1 ';

comment on column t_form_apply_field_value.attribute2 is
'扩展属性二';

comment on column t_form_apply_field_value.version is
'更新版本号';

/*==============================================================*/
/* Index: form_apply_field_idx_mainid                         */
/*==============================================================*/
create index form_apply_field_idx_mainid on t_form_apply_field_value (
  main_id ASC,
  field_control_id ASC
);
-- user
insert into t_sys_user (ID, USER_NAME, USER_ACCOUNT, PASSWORD, PHONE, EMAIL, LAN_CODE, CPY_CODE, ORG_CODE, AVATAR, WEIXIN, USER_STATUS, THIRD_ACCOUNT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION)
values ('sys_admin_1', '系统管理员', 'sys_admin', 'e66e322b4e953881542cc7cccf9429e9', '13480041729', 'hymanyun@163.com', 'zh_CN', 'sys_admin_cpy', '', '', '', 'UNLOCK', '', now(), now(), '', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1');


insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('menu_id_sys_admin', '系统管理员', '3', '', '', '', '0', '', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'SYS_ADMIN');


insert into t_sys_role (ID, ROLE_NAME, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION)
values ('sys_admin_role_id', '系统管理员角色', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '只有sys_admin', 'Y', 'SYS_ADMIN', '', '1');

insert into t_sys_role_user (ID, ROLE_ID, USER_ID, USER_ACCOUNT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION)
values ('role_user_sys_adminid', 'sys_admin_role_id', 'sys_admin_1', 'sys_admin', now(), now(), '', '', '', 'sys_admin_cpy', '', 'Y', '', '', '1');

insert into t_sys_role_menu (ID, ROLE_ID, MENU_ID, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION)
values ('role_menu_sys_adminid', 'sys_admin_role_id', 'menu_id_sys_admin', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1');


-- menu
insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('7589d21683c74d61a45c9f127c662035', '角色所有权限', '3', '5d53a62daa24433db9ca9d443e078b47', '', '', '1', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'ROLE_ALL');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040710045377161', '普通管理员', '3', '', '', '', '1', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'COMMON_ADMIN');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040314451396355', '表单定义所有权限', '3', 'd9b0d49ff6314488b3857ec06cf6ae77', '', '', '1', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '3', '0', '0', '', 'FORM_SET_ALL');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040314464499480', '表单定义创建', '3', 'd9b0d49ff6314488b3857ec06cf6ae77', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'FORM_SET_CREATE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040314470774321', '表单定义删除', '3', 'd9b0d49ff6314488b3857ec06cf6ae77', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'FORM_SET_DELETE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040314484024265', '表单定义注销', '3', 'd9b0d49ff6314488b3857ec06cf6ae77', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'FORM_SET_DESTORY');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040314490845882', '表单定义变更', '3', 'd9b0d49ff6314488b3857ec06cf6ae77', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'FORM_SET_CHANGE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040314494556824', '流程配置注销', '3', 'df2ae518e4ed4cd99986777cfe6da1bc', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'WORKFLOW_CONFIG_DESTORY');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040314500385302', '流程配置提交', '3', 'df2ae518e4ed4cd99986777cfe6da1bc', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'WORKFLOW_CONFIG_SUBMIT');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('10520041010110339872', '打印导出模板所有权限', '3', '10520041010101305200', '', '', '1', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'SYSPRINT_ALL');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('10520041010122389506', '打印导出模板查询', '3', '10520041010101305200', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'SYSPRINT_SELECT');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('10520041010455944772', '打印导出模板变更', '3', '10520041010101305200', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'SYSPRINT_CHANGE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('10520041010471444177', '打印导出模板提交', '3', '10520041010101305200', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'SYSPRINT_SUBMIT');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('10520041010473005513', '打印导出模板删除', '3', '10520041010101305200', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'SYSPRINT_DELETE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040211122281338', '职位所有权限', '3', '20040210552032330', '', '', '1', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'SYSJOB_ALL');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040314463046278', '表单定义查询', '3', 'd9b0d49ff6314488b3857ec06cf6ae77', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'FORM_SET_SELECT');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040314473710108', '表单定义提交', '3', 'd9b0d49ff6314488b3857ec06cf6ae77', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'FORM_SET_SUBMIT');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('10520041010454388136', '打印导出模板新增', '3', '10520041010101305200', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'SYSPRINT_CREATE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('10520041010474782297', '打印导出模板注销', '3', '10520041010101305200', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '0', '0', '', 'SYSPRINT_DESTORY');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('de04985ac27e48ebaf1228b6b731decc', ' 角色新增更改', '3', '5d53a62daa24433db9ca9d443e078b47', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'ROLE_CREATE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('d49f0de56a804f979fcd7b5bbaf789de', ' 角色删除', '3', '5d53a62daa24433db9ca9d443e078b47', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'ROLE_DELETE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('c31c40d702634b7cb5764fef6dfdb879', ' 角色查询', '3', '5d53a62daa24433db9ca9d443e078b47', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'ROLE_SELECT');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('fe8204dc9d0f42abb28795e3e29d4fd9', '组织机构所有权限', '3', '10c73c11b2604593afd285dbd4a10d1d', '', '', '1', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '13', '0', '0', '', 'ORGSET_ALL');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('b18941b784a1439c828d0a6ae0385546', '机构新增更改', '3', '10c73c11b2604593afd285dbd4a10d1d', '', '', '99', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '5', '0', '0', '', 'ORGSET_CREATE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('2487b45ce98a4db3a9901ea905f31895', '机构删除', '3', '10c73c11b2604593afd285dbd4a10d1d', '', '', '3', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '6', '0', '0', '', 'ORGSET_DELETE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('e0d85f75d62b4a558cb5c845f0057942', '机构查询', '3', '10c73c11b2604593afd285dbd4a10d1d', '', '', '2', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '6', '0', '0', '', 'ORGSET_SELECT');


insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040211124867327', '职位查询', '3', '20040210552032330', '', '', '2', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'SYSJOB_SELECT');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040211131205535', '职位删除', '3', '20040210552032330', '', '', '2', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'SYSJOB_DELETE');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040211134717622', '职位新增更改', '3', '20040210552032330', '', '', '2', '', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '0', '0', '', 'SYSJOB_CREATE');


insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('5d53a62daa24433db9ca9d443e078b47', '角色管理', '2', '83d15a067f7f455bacea8e5b4c639803', 'system/role/index', 'role', '15', 'role', '0', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '4', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('8861aea125444fdaa5d0ef082f42881d', '菜单管理', '2', '83d15a067f7f455bacea8e5b4c639803', 'system/menu/index', 'menu', '3', 'menu', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '2', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('10c73c11b2604593afd285dbd4a10d1d', '我的组织机构', '2', '83d15a067f7f455bacea8e5b4c639803', 'system/orgSet/index', 'orgSet', '10', 'dept', '0', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '3', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('638d1ad6b1fb4bb79861961c8cb8b484', '公司信息管理', '2', '83d15a067f7f455bacea8e5b4c639803', 'system/cpy/index', 'cpy', '0', 'peoples', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '3', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('66e6bb41dd5d4118815bcfa4679a4746', '字典管理', '2', '83d15a067f7f455bacea8e5b4c639803', 'system/dataDict/index', 'dataDict', '2', 'dictionary', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '2', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('3f07a740f9324a77885905eb387d8665', '系统监控', '2', '', '', 'monitor', '10', 'monitor', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('f581c07d7feb47859a09932ca6aaf3ab', '系统缓存', '2', '3f07a740f9324a77885905eb387d8665', 'monitor/redis/index', 'redis', '10', 'redis', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '2', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('cc79b766654e4c7591bdb38c68378d6e', 'SQL监控', '2', '3f07a740f9324a77885905eb387d8665', 'monitor/sql/index', 'druid', '99', 'sqlMonitor', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('736ba7c3fc804e6b81f4451938bab0e5', '系统工具', '2', '', '', 'sys-tools', '50', 'sys-tools', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('8546f3dd925f4d9885104904b6eeea50', '定时任务', '2', '736ba7c3fc804e6b81f4451938bab0e5', 'tools/job/index', 'job', '10', 'timing', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '4', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('c9d103445bb1478bbdea9cca8f2d1700', '接口文档', '2', '736ba7c3fc804e6b81f4451938bab0e5', 'tools/swagger/index', 'swagger2', '20', 'swagger', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('13dd7041975d4e21b0e8bd3a4e6247cb', '代码生成', '2', '736ba7c3fc804e6b81f4451938bab0e5', 'generator/index', 'generator', '30', 'dev', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('20040210552032330', '职位管理', '2', '83d15a067f7f455bacea8e5b4c639803', 'system/sysJob/index', 'sysJob', '5', 'people', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '3', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('10520041010101305200', '打印导出模板管理', '2', '83d15a067f7f455bacea8e5b4c639803', 'system/sysPrint/index', 'sysPrint', '30', 'chart', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('ceaa44a287c94dd3a1c55fee375840d2', '用户管理', '2', '83d15a067f7f455bacea8e5b4c639803', 'system/user/index', 'user', '4', 'user', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '4', '1', '0', '', '');

insert into t_sys_menu (ID, MENU_NAME, MENU_TYPE, PARENT_ID, COMPONENT_URL, PATH_URL, SORT, ICON, IS_ADMIN, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, IS_VISABLE, IS_WORKFLOW, MODULE_BUS_CLASS, PRIV_CODE)
values ('83d15a067f7f455bacea8e5b4c639803', '系统管理', '2', '', '', 'system', '4', 'system', '0', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '10', '1', '0', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('d922d6d4ea984993bdadf7efb781b129', 'cpy_type', 'zh_CN', '1', '有限公司', '1', now(), now(), 'admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '3', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('d78ddc65f4634a499ba129192e8459d1', 'cpy_status', 'zh_CN', 'ONLINE', '在线', '2', now(), now(), 'admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '11', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('90b7c901ee0948af984243af181f3e3b', 'cpy_status', 'zh_CN', 'OFFLINE', '离线', '1', now(), now(), 'admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '13', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('497082844f9d4021b115ea63a6cff77f', 'cpy_type', 'zh_CN', '2', '股份有限公司', '2', now(), now(), 'admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '3', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('cce372f042994ee8879b4f087f8836ce', 'user_status', 'zh_CN', 'UNLOCK', '激活', '1', now(), now(), 'admin', 'admin', '', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('0cf5bd2c3ba3451a907ab523f0991fdc', 'user_status', 'zh_CN', 'LOCK', '锁定', '1', now(), now(), 'admin', 'admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('a359428af3364aeb913bbf789eaba57e', 'flow_state', 'zh_CN', '2', '挂起', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('181abf594df9415db696d4a52e29c928', 'form_priv_type_code', 'zh_CN', 'USER', '用户', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('d5a845a0d958469d948bbc94aa54c932', 'form_priv_type_code', 'zh_CN', 'ROLE', '角色', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('24920a9af89243e6b7fe6a2b4e9d993f', 'flow_vote_method', 'zh_CN', 'NUM', '绝对票数', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('3d47a9bd172241898f9c59b761ac1000', 'flow_vote_method', 'zh_CN', 'PERCENT', '百分比', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('6799cb22bdce4ba99377355a659ea8fe', 'menu_type', 'zh_CN', '3', '权限标识', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('20040311400319001', 'job_type', 'zh_CN', 'MGR', '管理员', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('20040311402432776', 'job_type', 'zh_CN', 'TECH', '技术类', '10', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('20040311403973989', 'job_type', 'zh_CN', 'COMMON', '普工类', '20', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('20040311405141222', 'job_type', 'zh_CN', 'OTHER', '其它', '30', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('2cda20cb063745d98508dff1fada1b11', 'menu_type', 'zh_CN', '1', '外部菜单', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('64c73750626f4715a3a69524fc17cad2', 'menu_type', 'zh_CN', '2', '内部菜单', '2', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('bff7493858b642ba86aa1a0187fce397', 'df23323', 'zh_CN', 'df', 'df', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('ad5d07a4875644a08f2b3f93d6cce745', 'flow_state', 'zh_CN', '1', '有效', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('c4886a0baf574eaba6b86ebf9e07d778', 'lan_code', 'zh_CN', 'en_US', '英文', '2', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('f97742eb77a34fa2ac31e03ffe651510', 'flow_approve_type', 'zh_CN', 'USER', '用户', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('d7146c88504d4921a54a496149b987e3', 'flow_approve_type', 'zh_CN', 'ROLE', '角色', '2', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('1f3f70bcfa814b349d4ffa9f18f48724', 'flow_approve_type', 'zh_CN', 'MGR', '直属上司', '2', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('a319fa1c4ac34f68a05be2deb53042f9', 'org_type', 'zh_CN', '3', '班组', '2', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('9c645a0bc5dc44dd85438bb5d4fd019b', 'enable_flag', 'zh_CN', 'Y', '有效', '3', now(), now(), 'admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '3', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('e5ecf5053b214046abc5861a53999ea4', 'enable_flag', 'zh_CN', 'N', '无效', '2', now(), now(), 'admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '4', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('2ed6d53becfd49f29ac9ab45aa4bd4c9', 'lan_code', 'zh_CN', 'zh_CN', '中文', '1', now(), now(), 'admin', 'admin', '', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('d4d37d2158e8495b861c7076bf3460a6', 'yesorno', 'zh_CN', '1', '是', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '3', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('92f8471b5460479285bd54c760128076', 'yesorno', 'zh_CN', '0', '否', '1', now(), now(), 'sys_admin', 'sys_admin', '', 'sys_admin_cpy', '', 'Y', '', '', '3', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('9948e402497b435b8403e40e3a5d75b5', 'org_type', 'zh_CN', '1', '部门', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('751a32142a554f2ea8b880ed90b82559', 'org_type', 'zh_CN', '2', '用户', '4', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('5e4455a745ab4db082e1fadb8c722487', 'flow_state', 'zh_CN', '0', '待部署', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '2', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('9941fd66b66d4c4ea4c0832e43d843f5', 'bus_self_state', 'zh_CN', '0', '草稿', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('ca6134d78a4c4fdfa388c0afacf89c82', 'bus_self_state', 'zh_CN', '1', '待审批', '2', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('5dbe5e9f4aec4c60b908cde04244172c', 'bus_self_state', 'zh_CN', '10', '有效', '0', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('aef1bfc8db5a46e2bd46717691721104', 'bus_self_state', 'zh_CN', '4', '审批拒绝', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', '', '', '1', '', '', '', '', '', '');

insert into t_sys_data_dict (ID, DICT_TYPE, LAN_CODE, DICT_CODE, DICT_NAME, SORT, CREATE_DATE, LAST_UPDATE_DATE, CREATE_BY, LAST_UPDATE_BY, SYS_ORG_CODE, SYS_OWNER_CPY, RMK, ENABLE_FLAG, ATTRIBUTE1, ATTRIBUTE2, VERSION, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8)
values ('2fb5d198c2f54cf49fce7f91d1b7ef61', 'report_print_type', 'zh_CN', 'cpy_report_code', '公司报表代码测试', '1', now(), now(), 'sys_admin', 'sys_admin', 'ORG19072518404426381', 'sys_admin_cpy', '', 'Y', 'classpath:jxls/export/admin/template/cpy_template.xlsx', 'cpyJxlsExportSupport', '2', '', '', '', '', '', '');


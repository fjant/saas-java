/*==============================================================*/
/* Table: t_bus_common_state                                    */
/*==============================================================*/
CREATE TABLE t_bus_common_state
(
   id                  VARCHAR(40) NOT NULL COMMENT '主键(sys_code+UUID)',
   main_id              VARCHAR(40) NOT NULL COMMENT '主表ID',
   self_state           INT(2) NOT NULL COMMENT '自身单据状态',
   other_state          INT(2) COMMENT '对方接受的单据状态',
   bus_type             INT(2) COMMENT '业务类别(即时生效或待对方同意生效)',
   bus_date             datetime COMMENT ' 业务时间',
   bus_direction        INT(2) COMMENT '业务方向(1:主动发起, 2: 被接受)',
   bus_module           varchar(10) COMMENT '业务模块',
   bus_class            varchar(20) COMMENT '业务类型',
   fun_process          INT(2) COMMENT '功能处理(新增，变更，注销)',
   rel_num              varchar(40) COMMENT '关联号(同一张单据的匹配号)',
   encrypt_code         varchar(100) COMMENT '加密数据串(防止数据被更改)',
   forward_type         INT(2) COMMENT '转发类别(0:不转发,1:统一转发，2:转发给对应的当事人)',
   parent_id            varchar(40) COMMENT '原ID（变更，注销对应的原ID）',
   custom_code          varchar(100) COMMENT '自定义编号',
   sys_gen_code         varchar(100) COMMENT '系统产生编号',
   create_date          datetime NOT NULL,
   last_update_date     datetime COMMENT '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) COMMENT '最后更新人',
   sys_org_code         varchar(40) COMMENT '用户所属组织部门',
   sys_owner_cpy        varchar(40) COMMENT '用户所属公司',
   other_sys_org_code   varchar(40) COMMENT '对方组织',
   other_sys_owner_cpy  varchar(40) COMMENT '对方公司',
   rmk                  varchar(1000) COMMENT '备注',
   enable_flag          CHAR(1) COMMENT '有效标识',
   attribute1           varchar(100) COMMENT '扩展属性1 ',
   attribute2           varchar(100) COMMENT '扩展属性二',
   history_flag         varchar(1) COMMENT '历史标识',
   bus_version          INT(3) COMMENT '业务版本号',
   VERSION              INT(8) COMMENT '更新版本号',
   PRIMARY KEY (id),
   INDEX bus_common_state_idx_mainid (main_id),
   INDEX bus_common_state_idx_cpy (sys_owner_cpy,history_flag,self_state)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

ALTER TABLE t_bus_common_state COMMENT '业务状态表(模块公用表)';

/*==============================================================*/
/* Table: t_bus_common_state                                    */
/*==============================================================*/
CREATE TABLE th_bus_common_state
(
   id                  VARCHAR(40) NOT NULL COMMENT '主键(sys_code+UUID)',
   main_id              VARCHAR(40) NOT NULL COMMENT '主表ID',
   self_state           INT(2) NOT NULL COMMENT '自身单据状态',
   other_state          INT(2) COMMENT '对方接受的单据状态',
   bus_type             INT(2) COMMENT '业务类别(即时生效或待对方同意生效)',
   bus_date             datetime COMMENT ' 业务时间',
   bus_direction        INT(2) COMMENT '业务方向(1:主动发起, 2: 被接受)',
   bus_module           varchar(10) COMMENT '业务模块',
   bus_class            varchar(20) COMMENT '业务类型',
   fun_process          INT(2) COMMENT '功能处理(新增，变更，注销)',
   rel_num              varchar(40) COMMENT '关联号(同一张单据的匹配号)',
   encrypt_code         varchar(100) COMMENT '加密数据串(防止数据被更改)',
   forward_type         INT(2) COMMENT '转发类别(0:不转发,1:统一转发，2:转发给对应的当事人)',
   parent_id            varchar(40) COMMENT '原ID（变更，注销对应的原ID）',
   custom_code          varchar(100) COMMENT '自定义编号',
   sys_gen_code         varchar(100) COMMENT '系统产生编号',
   create_date          datetime NOT NULL,
   last_update_date     datetime COMMENT '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) COMMENT '最后更新人',
   sys_org_code         varchar(40) COMMENT '用户所属组织部门',
   sys_owner_cpy        varchar(40) COMMENT '用户所属公司',
   other_sys_org_code   varchar(40) COMMENT '对方组织',
   other_sys_owner_cpy  varchar(40) COMMENT '对方公司',
   rmk                  varchar(1000) COMMENT '备注',
   enable_flag          CHAR(1) COMMENT '有效标识',
   attribute1           varchar(100) COMMENT '扩展属性1 ',
   attribute2           varchar(100) COMMENT '扩展属性二',
   history_flag         varchar(1) COMMENT '历史标识',
   bus_version          INT(3) COMMENT '业务版本号',
   VERSION              INT(8) COMMENT '更新版本号',
   PRIMARY KEY (id),
   INDEX bus_common_state_idxh_mainid (main_id),
   INDEX bus_common_state_idxh_cpy (sys_owner_cpy,history_flag,self_state)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

ALTER TABLE th_bus_common_state COMMENT '业务状态历史表(模块公用表)';

/*==============================================================*/
/* Table: t_sys_attachment                                      */
/*==============================================================*/
create table t_sys_attachment
(
   id                   varchar(40) not null comment '主键(sys_code+UUID)',
   main_id              varchar(40) comment '主表ID',
   bus_module           varchar(10) comment '业务模块',
   file_name            varchar(100) comment '文件名',
   file_type            varchar(6) comment '文件类型',
   file_size            int(8) comment '文件大小(Byte)',
   file_path            varchar(100) comment '文件路径',
   file_url             varchar(200) comment '文件访问URL',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   attribute1           varchar(100) comment '扩展属性1 ',
   attribute2           varchar(100) comment '扩展属性二',
   version              int(8) comment '更新版本号',
   primary key (id),
   INDEX sys_attachment_idx_mainid (main_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

alter table t_sys_attachment comment '附件表';

/*==============================================================*/
/* Table: t_bus_extend                                          */
/*==============================================================*/
create table t_bus_extend
(
   id                   varchar(40) not null comment '主键',
   main_id              varchar(40) comment '主表ID',
   form_apply_id        varchar(40) comment '表单申请Id',
   source_form_apply_id varchar(40) comment '表单申请ID(复制或变更带过来的ID)',
   form_id              varchar(40) comment '表单设置ID',
   form_code            varchar(40) comment '表单编码',
   bus_Module           varchar(10) comment '模块',
   bus_class            varchar(10) comment '业务种类',
   create_date          datetime not null,
   last_update_date     datetime comment '最后更新时间',
   create_by            varchar(40),
   last_update_by       varchar(40) comment '最后更新人',
   sys_org_code         varchar(40) comment '用户所属组织部门',
   sys_owner_cpy        varchar(40) comment '用户所属公司',
   rmk                  varchar(1000) comment '备注',
   enable_flag          char(1) comment '有效标识',
   version              numeric(8,0),
   attribute1           varchar(400) comment '扩展属性1 ',
   attribute2           varchar(400) comment '扩展属性二',
   attribute3           varchar(400),
   attribute4           varchar(400),
   attribute5           varchar(400),
   attribute6           varchar(400),
   attribute7           numeric(26,6) comment '更新版本号',
   attribute8           numeric(26,6),
   attribute9           datetime,
   attribute10          datetime,
   primary key (id)
);

alter table t_bus_extend comment '业务扩展表(与表单进行关联)';

/*==============================================================*/
/* Index: bus_extend_idx_mainid                                 */
/*==============================================================*/
create index bus_extend_idx_mainid on t_bus_extend
(
   main_id,
   enable_flag
);



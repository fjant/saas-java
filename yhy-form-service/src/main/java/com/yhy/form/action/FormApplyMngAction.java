package com.yhy.form.action;

import com.yhy.common.action.BaseSimpleMngAction;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.service.BusCommonStateService;
import com.yhy.common.utils.SpringContextHolder;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.BusCommonState;
import com.yhy.form.dto.FormApplyDTO;
import com.yhy.form.service.FormApplyFieldValueService;
import com.yhy.form.service.mng.FormApplyMngService;
import com.yhy.form.vo.FormApplyFieldValueVO;
import com.yhy.form.vo.mng.FormApplyMngVO;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-17 上午11:42 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@RestController
@RequestMapping(value="/form-service/api/formApplyMng", produces="application/json;charset=UTF-8")
public class FormApplyMngAction extends BaseSimpleMngAction<FormApplyDTO> {

    @Autowired
    private FormApplyFieldValueService applyFieldValueService;
    @Autowired
    private BusCommonStateService busCommonStateService;

    @Override
    protected FormApplyMngService getBaseService() {
        return SpringContextHolder.getBean(FormApplyMngService.class);
    }

    @Override
    protected void afterLoadDataInfo(FormApplyDTO baseDTO, AppReturnMsg returnMsg ) {

    }

    protected void beforeOfViewApprove(FormApplyDTO baseDTO) {
        FormApplyMngVO paramBean = baseDTO.getParamBean();
        BusCommonState busCommonState = busCommonStateService.findByBusModuleAndId(getBaseService().getBusModule(), paramBean.getBusId());
        paramBean.setId(busCommonState.getMainId());
    }

    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormApplyDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody FormApplyDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormApplyDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody FormApplyDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/toChange",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="变更操作", notes="变更操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormApplyDTO.class)
    })
    public AppReturnMsg toChange(HttpServletRequest request, HttpServletResponse response, @RequestBody FormApplyDTO baseDTO) {
        return super.toChangeCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataTypeClass = FormApplyDTO.class)
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody FormApplyDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataTypeClass = FormApplyDTO.class)
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody FormApplyDTO baseDTO) {
        baseDTO.getParamBean().setCreateBy(YhyUtils.getSysUser().getUserAccount());
        return super.loadDataCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataTypeClass = FormApplyDTO.class)
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody FormApplyDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/doSubmit",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="提交", notes="提交操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormApplyDTO.class)
    })
    public AppReturnMsg doSubmit(HttpServletRequest request, HttpServletResponse response, @RequestBody FormApplyDTO baseDTO) {
        return super.doSubmitCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/doDestory",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="注销操作", notes="注销操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormApplyDTO.class)
    })
    public AppReturnMsg doDestory(HttpServletRequest request, HttpServletResponse response, @RequestBody FormApplyDTO baseDTO) {
        return super.doDestoryCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/getApplyFieldValueInfo",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value="根据主ID获取表单字段值", notes="根据主ID获取表单字段值")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mainId", value = "主表ID", required = true, dataTypeClass = String.class)
    })
    public AppReturnMsg getFieldPrivInfo(HttpServletRequest request, HttpServletResponse response,
                                         @RequestParam("mainId") String mainId) {
        List<FormApplyFieldValueVO> fieldValueVOS = applyFieldValueService.findByMainId(mainId);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),null,fieldValueVOS);
    }

}
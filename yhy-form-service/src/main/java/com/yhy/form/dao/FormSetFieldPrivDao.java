package com.yhy.form.dao;

import com.yhy.common.dao.BaseDao;
import com.yhy.form.vo.FormSetFieldPrivVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午11:43 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "formSetFieldPrivDao")
public interface FormSetFieldPrivDao extends BaseDao<FormSetFieldPrivVO> {

    List<FormSetFieldPrivVO> findByMainId(@Param("mainId") String mainId);

    List<FormSetFieldPrivVO> findByFieldId(@Param("fieldId") String fieldId);

}

package com.yhy.job.config;

import com.yhy.common.utils.ITransationExecutor;
import com.yhy.common.utils.TransationExecuteContext;
import com.yhy.job.service.mng.QuartzJobMngService;
import com.yhy.job.utils.QuartzJobManage;
import com.yhy.job.vo.mng.QuartzJobMngVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-16 下午3:04 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Component
public class QuartzJobConfigStart implements ApplicationRunner {

    protected Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private QuartzJobMngService quartzJobMngService;

    @Autowired
    private QuartzJobManage quartzJobManage;

    /**
     * 项目启动时重新激活启用的定时任务
     * @param applicationArguments
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments applicationArguments){
        List<QuartzJobMngVO> quartzJobs = quartzJobMngService.findAllValidJob();
        quartzJobs.forEach(quartzJob -> {
            boolean result = TransationExecuteContext.getTransationExecuteContext().execute(new ITransationExecutor<Boolean, QuartzJobMngVO>() {
                @Override
                public Boolean execute(QuartzJobMngVO quartzJob) {
                    quartzJobManage.addJob(quartzJob);
                    //quartzJobService.update(JsonUtils.tranObject(quartzJob,QuartzJobVO.class));
                    LOGGER.info("任务更改状态为Running，任务名称：{}", quartzJob.getJobName());
                    return true;
                }
            }, quartzJob);
        });
        LOGGER.info("--------------------定时任务注入完成---------------------");
    }

}

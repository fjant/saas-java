package com.yhy.admin.service;

import com.yhy.admin.dao.SysPrintMainDao;
import com.yhy.common.service.BaseMainService;
import com.yhy.common.vo.SysPrintMainVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-31 下午4:47 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class SysPrintMainService extends BaseMainService<SysPrintMainVO> {

	@Autowired
	private SysPrintMainDao sysPrintMainDao;

	@Override
	protected SysPrintMainDao getDao() {
		return sysPrintMainDao;
	}


}

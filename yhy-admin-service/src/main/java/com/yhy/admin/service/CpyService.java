package com.yhy.admin.service;

import com.yhy.admin.dao.CpyDao;
import com.yhy.admin.dao.DataDictDao;
import com.yhy.admin.vo.CpyVO;
import com.yhy.admin.vo.DataDictVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.service.BaseMainService;
import com.yhy.common.utils.YhyUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-28 上午10:01 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CpyService extends BaseMainService<CpyVO> {

    @Autowired
    private CpyDao cpyDao;

    @Override
    protected CpyDao getDao() {
        return cpyDao;
    }

    @Override
    protected void preInsert(CpyVO entity) {
        super.preInsert(entity);
        if(StringUtils.isBlank(entity.getCpyCode())) {
            entity.setCpyCode(YhyUtils.genSysCode(BusModuleType.MD_CPY_MNG.getVal()));
        }
    }
}

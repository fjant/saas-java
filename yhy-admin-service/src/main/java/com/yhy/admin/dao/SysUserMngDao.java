package com.yhy.admin.dao;

import com.yhy.admin.dto.SysUserDTO;
import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.common.dao.BaseMngDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-7-3 上午10:32 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Mapper
@Component(value = "sysUserMngDao")
public interface SysUserMngDao extends BaseMngDao<SysUserDTO,SysUserMngVO> {

    SysUserMngVO checkUserAndPassword(@Param("userAccount") String userAccount, @Param("password") String password);

    SysUserMngVO findByPhone(@Param("phone") String phone);

    SysUserMngVO findByUserAccount(@Param("userAccount") String userAccount);

    SysUserMngVO findByUserCpyCode(@Param("userAccount") String userAccount, @Param("cpyCode")String cpyCode);

    List<SysUserMngVO> findUserByOrgCode(@Param("cpyCode")String cpyCode, @Param("orgCode")String orgCode);

    List<SysUserMngVO> findUserByCpyCode(@Param("cpyCode")String cpyCode);

}

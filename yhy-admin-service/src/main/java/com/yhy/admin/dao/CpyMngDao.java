package com.yhy.admin.dao;

import com.yhy.admin.dto.CpyDTO;
import com.yhy.admin.vo.mng.CpyMngVO;
import com.yhy.common.dao.BaseMngDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-28 上午10:17 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Mapper
@Component(value = "cpyMngDao")
public interface CpyMngDao extends BaseMngDao<CpyDTO,CpyMngVO> {


}

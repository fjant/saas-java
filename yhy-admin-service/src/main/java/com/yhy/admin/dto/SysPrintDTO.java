package com.yhy.admin.dto;

import com.yhy.admin.vo.mng.SysPrintMngVO;
import com.yhy.common.dto.BaseMngDTO;
import com.yhy.common.vo.SysPrintMainVO;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-31 下午5:15 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

public class SysPrintDTO extends BaseMngDTO<SysPrintMngVO> {


    @Override
    public SysPrintMainVO getBusMain() {
        return new SysPrintMainVO();
    }


}

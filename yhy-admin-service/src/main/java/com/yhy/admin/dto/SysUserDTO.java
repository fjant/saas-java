package com.yhy.admin.dto;

import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.common.dto.BaseMngDTO;
import com.yhy.common.vo.SysUser;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-7-3 上午10:32 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

public class SysUserDTO extends BaseMngDTO<SysUserMngVO> {

    @Override
    public SysUser getBusMain() {
        return new SysUser();
    }

}

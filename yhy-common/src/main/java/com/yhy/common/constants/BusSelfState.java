package com.yhy.common.constants;

import com.yhy.common.exception.BusinessException;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
public enum BusSelfState {

	/**
	 * 草稿
	 */
	DRAFT("0", "bus.state.draft"),

	/**
	 * 删除
	 */
	DELETED("-1","bus.state.deleted"),

	/**
	 * 待审批
	 */
	WAIT("1", "bus.state.wait"),

	/**
	 * 审批不通过
	 */
	APPROVE_NOT_PASS("4","bus.state.approve_not_pass"),

	/**
	 * 无效
	 */
	INVALID("-10", "bus.state.invalid"),

	/**
	 * 有效
	 */
	VALID("10", "bus.state.valid");

	private final String val;
	private final String labCode;

	private static Map<String, BusSelfState> busStateMap;

	BusSelfState(String val, String labCode) {
		this.val = val;
		this.labCode = labCode;
	}

	public String getVal() {
		return val;
	}

	public String getLabel() {
		return labCode;
	}

	public static BusSelfState getInstByVal(String key) {
		if (busStateMap == null) {

			synchronized (BusSelfState.class) {
				if (busStateMap == null) {
					busStateMap = new HashMap<String, BusSelfState>();
					for (BusSelfState busState : BusSelfState.values()) {
						busStateMap.put(busState.getVal(), busState);
					}
				}
			}
		}
		if (!busStateMap.containsKey(key)) {
			throw new BusinessException("状态值" + key + "对应的BusSelfState枚举值不存在。");
		}
		return busStateMap.get(key);
	}

}

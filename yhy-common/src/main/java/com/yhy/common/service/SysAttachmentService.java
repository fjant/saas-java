package com.yhy.common.service;

import com.yhy.common.dao.SysAttachmentDao;
import com.yhy.common.vo.SysAttachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-9-2 下午3:29 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysAttachmentService extends BaseService<SysAttachment> {
    @Autowired
    private SysAttachmentDao sysAttachmentDao;

    @Override
    protected SysAttachmentDao getDao() {
        return sysAttachmentDao;
    }

    public SysAttachment findByBusModuleAndId(String busModule, String id) {
        return getDao().findByBusModuleAndId(busModule,id);
    }


    public void deleteByMainId(String busModule,String mainId) {
        getDao().deleteByMainId(busModule, mainId);
    }

    public List<SysAttachment> findByMainId(String busModule, String mainId) {
        return getDao().findByMainId(busModule, mainId);
    }

    @Override
    protected void preInsert(SysAttachment entity) {
        super.preInsert(entity);
    }

}

package com.yhy.common.service;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.yhy.common.api.IWorkflowService;
import com.yhy.common.constants.*;
import com.yhy.common.dao.BaseComplexMngDao;
import com.yhy.common.dao.BaseMngDao;
import com.yhy.common.dto.BaseMainEntity;
import com.yhy.common.dto.BaseMngDTO;
import com.yhy.common.dto.BaseMngVO;
import com.yhy.common.dto.workflow.WorkflowInfo;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.utils.BusCenterHelper;
import com.yhy.common.utils.JsonUtils;
import com.yhy.common.utils.SpringContextHolder;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.BusCommonState;
import com.yhy.common.vo.BusExtendVO;
import com.yhy.common.vo.SysAttachment;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-12-12 上午11:47 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public abstract class BaseComplexMngService<T extends BaseMngDTO, E extends BaseMngVO> extends BaseMngService<T, E> {

    protected abstract BaseComplexMngDao getBaseMngDao();


    public List<E> findHistoryDataBySysCode(String sysOwnerCpy, String sysGenCode) {
        return getBaseMngDao().findHistoryDataBySysCode(sysOwnerCpy, sysGenCode, getBusModule());
    }

    @Override
    protected Integer processMainOfDelete(BaseMainEntity baseMainEntity, E busMainData) {
        // 复杂业务与简单业务处理
        if(!FunProcess.DESTORY.getVal().equals(busMainData.getFunProcess())) {
            return getBaseMainService().deleteById(baseMainEntity);
        }
        return getBaseMainService().updateVersion(baseMainEntity);
    }

    protected void processBusCommonStateOfDelete(E busMainData) {
        //2.删除业务表
        getBusCommonStateService().deleteCommonStateById(busMainData.getBusId(),getBusModule());
        if(StringUtils.isNotBlank(busMainData.getParentId())) {
            //处理历史标识, 恢复业务状态表上一版本的 history_flag 为N
            getBusCommonStateService().updateRecoverCommonState(getBusModule(),busMainData.getParentId());
        }
    }

    @Override
    protected void processBusCommonStateOfSave(T baseDTO, E busMainData) {
        BusCommonState busCommonState = baseDTO.getBusCommonState();
        if (OperateType.EDIT.getVal().equals(baseDTO.getOperateType())) {
            BusCommonState updateBusCommonState = getBusCommonStateService().findByBusModuleAndId(getBusModule(), busCommonState.getId());
            updateBusCommonState.setBusType(busCommonState.getBusType());
            updateBusCommonState.setBusDate(busCommonState.getBusDate());
            updateBusCommonState.setCustomCode(busCommonState.getCustomCode());
            getBusCommonStateService().update(updateBusCommonState);
        } else {
            busCommonState.setId(null);
            busCommonState.setBusVersion(1);
            if(StringUtils.isNotBlank(busCommonState.getParentId())) {
                BusCommonState prevBusCommonState = getBusCommonStateService().findByBusModuleAndId(getBusModule(), busCommonState.getParentId());
                //更新前一版本数据为无效
                getBusCommonStateService().updateHistoryCommonState(getBusModule(),prevBusCommonState.getId());
                //业务版本号+1
                busCommonState.setBusVersion(prevBusCommonState.getBusVersion() + 1);
            }
            busCommonState.setSelfState(BusSelfState.DRAFT.getVal());
            busCommonState.setMainId(busMainData.getId());
            busCommonState.setBusModule(getBusModule());
            busCommonState.setBusClass(getBusClass());
            busCommonState.setOtherState(BusOtherState.VALID.getVal());
            busCommonState.setHistoryFlag("N");
            if(StringUtils.isBlank(busCommonState.getFunProcess())) {
                busCommonState.setFunProcess(FunProcess.NEW.getVal());
            }
            if (BusCenterHelper.isNeedFeedback(getBusModule())) {
                busCommonState.setOtherState(BusOtherState.WAIT_FEEDBACK.getVal());
            }
            getBusCommonStateService().insert(busCommonState);
            busMainData.setBusId(busCommonState.getId());
        }
    }

    protected void beforeDestoryOfProcess(E busMainData) {

    }

    protected void afterProcessCustomDataOfDestory(E busMainData) {

    }

    @Override
    protected BusSelfState startWorkflowOfBusiness(String userAccount, E busMainData) {
        //return BusSelfState.VALID;
        IWorkflowService workflowService = SpringContextHolder.getBean(IWorkflowService.SERVICE_BEAN);
        Map<String,Object> paramMap = Maps.newHashMap();
        paramMap.put("busMainData",busMainData);
        WorkflowInfo workflowInfo = new WorkflowInfo();
        workflowInfo.setPassByDirect(busMainData.getPassDirect());
        workflowInfo.setBusinessKey(busMainData.getBusId());
        workflowInfo.setBusModule(getBusModule());
        workflowInfo.setBusClass(getBusClass());
        if(BusCenterHelper.isOrgsetMng(getBusModule())) {
            workflowInfo.setSysOrgCode(busMainData.getSysOrgCode());
        }
        workflowInfo.setSysOwnerCpy(busMainData.getSysOwnerCpy());
        beforeSubmitOfWorkflowInfo(workflowInfo, busMainData);
        return workflowService.submitWorkflow(userAccount, workflowInfo, paramMap);
    }

    public void doSingleDestory(T baseDTO, E busMainData) {
        RedisLock redisLock = null;
        try {
            if(!BusSelfState.VALID.getVal().equals(busMainData.getSelfState())) {
                throw new BusinessException("请选择有效状态的记录进行注销!");
            }
            if(FunProcess.DESTORY.getVal().equals(busMainData.getFunProcess())) {
                throw new BusinessException("请选择非注销记录进行注销!");
            }
            if(!"N".equals(busMainData.getHistoryFlag())) {
                throw new BusinessException("请选择最新版本进行注销!");
            }
            //防止同步操作
            String redisLockKey = busMainData.getSysOwnerCpy() + (BusCenterHelper.isComplexBus(getBusModule()) ? busMainData.getSysGenCode() : busMainData.getId());
            if (StringUtils.isNotBlank(redisLockKey)) {
                redisLock = new RedisLock(redisLockKey, 200L); //100ms
                if (!redisLock.lock()) {
                    LOGGER.error("注销的数据已发生变化，请刷新页面重试." + redisLockKey);
                    throw new BusinessException("注销的数据已发生变化，请刷新页面重试.");
                }
            }
            beforeDestoryOfProcess(busMainData);

            SysUser sysUser = YhyUtils.getSysUser();

            BaseMainEntity baseMainEntity = JsonUtils.tranObject(busMainData, baseDTO.getBusMain().getClass());
            int updateNum = getBaseMainService().updateVersion(baseMainEntity);
            if(updateNum == 0) {
                LOGGER.error("注销时数据已发生变化，请刷新页面." + JsonUtils.toJson(baseMainEntity));
                throw new BusinessException("数据已发生变化，请刷新页面.");
            }
            BusCommonState insertBusCommonState = getBusCommonStateService().findByBusModuleAndId(getBusModule(),busMainData.getBusId());
            insertBusCommonState.setId(YhyUtils.genId(getBusModule()));
            insertBusCommonState.setParentId(busMainData.getBusId());
            insertBusCommonState.setFunProcess(FunProcess.DESTORY.getVal());
            insertBusCommonState.setBusDate(YhyUtils.getCurDateTime());
            insertBusCommonState.setBusVersion(insertBusCommonState.getBusVersion() + 1);
            insertBusCommonState.setRmk(busMainData.getBusRmk());

            String oldBusId = busMainData.getBusId();
            busMainData.setBusId(insertBusCommonState.getId());//更新为当前注销记录 ID
            busMainData.setParentId(oldBusId);
            busMainData.setFunProcess(FunProcess.DESTORY.getVal());
            busMainData.setBusDate(insertBusCommonState.getBusDate());

            BusSelfState busSelfState = BusSelfState.DRAFT;
            if(baseDTO.getListSubmit()) {
                //提交时启动工作流
                busSelfState = this.startWorkflowOfBusiness(sysUser.getUserAccount(), busMainData);
            }
            insertBusCommonState.setSelfState(busSelfState.getVal());
            busMainData.setSelfState(busSelfState.getVal());

            //更新被注销的业务表历史状态标识
            getBusCommonStateService().updateHistoryCommonState(getBusModule(),oldBusId);
            //插入一条注销的业务状态记录
            getBusCommonStateService().insert(insertBusCommonState);

            afterProcessCustomDataOfDestory(busMainData);

            if(BusSelfState.VALID == busSelfState) {
                processBusCommonStateOfValid(busMainData);
            }

        } finally {
            if (redisLock != null) {
                redisLock.unlock();
            }
        }
    }

    @Override
    public void processBusCommonStateOfValid(E busMainData) {
        super.processBusCommonStateOfValid(busMainData);

        if(StringUtils.isNotBlank(busMainData.getParentId())) {
            //记录生效将上一版本的状态置为无效
            getBusCommonStateService().updateBusInvalid(busMainData.getParentId(),getBusModule());
        }
        // 将注销的状态直接置为 无效
        if(FunProcess.DESTORY.getVal().equals(busMainData.getFunProcess())) {
            getBusCommonStateService().updateBusState(busMainData.getBusId(), getBusModule(), BusSelfState.INVALID);
        }
    }

    public void toChangeData(T baseDTO) {
        preSetCommonBaseDto(baseDTO);
        E busMainData = findByBusId(baseDTO.getParamBean().getId());
        if(!BusSelfState.VALID.getVal().equals(busMainData.getSelfState())) {
            throw new BusinessException("当前记录未生效，不能变更");
        }
        if(FunProcess.DESTORY.getVal().equals(busMainData.getFunProcess())) {
            throw new BusinessException("选择新建或变更的记录进行变更");
        }
        if(!"N".equals(busMainData.getHistoryFlag())) {
            throw new BusinessException("请选择最新版本进行变更!");
        }
        baseDTO.setFunProcess(FunProcess.CHANGE.getVal());
        baseDTO.setOperateType(OperateType.CREATE.getVal());
        busMainData.setFunProcess(FunProcess.CHANGE.getVal());
        busMainData.setSelfState(BusSelfState.DRAFT.getVal());
        busMainData.setParentId(busMainData.getBusId());
        String oldId = busMainData.getId();
        busMainData.setOldMainId(oldId);
        busMainData.setId(null);

        baseDTO.setBusMainData(busMainData);
        if (BusCenterHelper.isComplexBus(getBusModule())) {
            BusCommonState busCommonState = JsonUtils.tranObject(baseDTO.getBusMainData(), BusCommonState.class);
            busCommonState.setId(null);
            busCommonState.setParentId(busMainData.getBusId());
            busCommonState.setBusDate(YhyUtils.getCurDateTime());
            baseDTO.setBusCommonState(busCommonState);
        }

        //业务扩展表
        BusClassType busClassType = BusClassType.getInstByVal(getBusClass());
        if(busClassType != null && busClassType.getExtendMng()) {
            BusExtendVO busExtend = getBusExtendService().findByBusModuleAndMainId(getBusModule(), busMainData.getOldMainId());
            busExtend = busExtend == null ? new BusExtendVO() : busExtend;
            busExtend.setId(null);
            busExtend.setMainId(null);
            String formApplyId = busExtend.getFormApplyId();
            busExtend.setSourceFormApplyId(formApplyId);
            baseDTO.setBusExtend(busExtend);
        }

        //附件
        if (BusCenterHelper.isAttachmentMng(getBusModule())) {
            List<SysAttachment> sysAttachments = getSysAttachmentService().findByMainId(getBusModule(),busMainData.getOldMainId());
            baseDTO.setAttachmentList(sysAttachments);
        }
        afterProcessCustomDataOfChange(baseDTO,busMainData);
    }

    protected void afterProcessCustomDataOfChange(T baseDTO, E busMainData) {

    }

}

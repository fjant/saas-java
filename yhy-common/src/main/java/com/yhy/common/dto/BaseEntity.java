package com.yhy.common.dto;

import com.yhy.common.utils.ToStringBean;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yanghuiyuan on 2017/11/7.
 */
public class BaseEntity extends ToStringBean implements Serializable {


    protected String id;

    private Integer version;


    private Date createDate;
    private Date lastUpdateDate;
    private String createBy;
    private String lastUpdateBy;
    private String rmk;
    private String enableFlag;
    private String attribute1;
    private String attribute2;
    private String sysOrgCode;
    private String sysOwnerCpy;

    public String getSysOrgCode() {
        return sysOrgCode;
    }

    public void setSysOrgCode(String sysOrgCode) {
        this.sysOrgCode = sysOrgCode;
    }

    public String getSysOwnerCpy() {
        return sysOwnerCpy;
    }

    public void setSysOwnerCpy(String sysOwnerCpy) {
        this.sysOwnerCpy = sysOwnerCpy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    //@JSONField(format="yyyy-MM-dd HH:mm:ss")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    //@JSONField(format="yyyy-MM-dd HH:mm:ss")
    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    public String getRmk() {
        return rmk;
    }

    public void setRmk(String rmk) {
        this.rmk = rmk;
    }

    public String getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(String enableFlag) {
        this.enableFlag = enableFlag;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }
}

package com.yhy.common.dao;

import com.yhy.common.dto.BaseDTO;
import com.yhy.common.dto.BaseMngVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-25 下午9:20 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

public interface BaseSimpleMngDao<T extends BaseDTO,E extends BaseMngVO> extends BaseMngDao<T, E> {


}

package com.yhy.common.dao;

import com.yhy.common.vo.BusCommonState;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
@Mapper
@Component(value = "busCommonStateDao")
public interface BusCommonStateDao extends BaseDao<BusCommonState> {

    Integer deleteCommonStateById(BusCommonState commonState);

    BusCommonState findByBusModuleAndId(@Param("busModule") String busModule,@Param("id") String id);

    BusCommonState findByBusModuleAndMainId(@Param("busModule") String busModule,@Param("mainId") String mainId);

    Integer updateBusState(BusCommonState commonState);

    Integer updateSimpleBusState(BusCommonState commonState);

    Integer updateHistoryCommonState(@Param("busModule") String busModule,@Param("id") String id);

    Integer updateRecoverCommonState(@Param("busModule") String busModule,@Param("id") String id);

}
